# MAME

arcademvs/MAME is a GitLab project related to unofficial patches and binaries for [MAME](https://github.com/mamedev/mame).

For full list of subgroups and projects visit [GitLab](https://gitlab.com/arcademvs).

Group Owner: Keil Miller Jr

[MAME License](LICENSE.md) is included in this repository. It must be included with any binary downloaded or distributed from this repository.

---

## Patches

Name | Abbreviation | Description | Author
:---: | :---: | :--- | :---:
d3d8 | 8 | Windows (old video card compatibility) | [Calamity](http://forum.arcadecontrols.com/index.php/topic,151459.0.html)
d3d9ex | 9 | Windows Vista+ (for less video lag) | [Calamity](http://forum.arcadecontrols.com/index.php/topic,151459.0.html)
groovymame | g | Fork aimed at CRT monitors | [Calamity](http://forum.arcadecontrols.com/index.php/topic,151459.0.html)
osx min 10.6 | - | Required to compile mame 0.202 on Mac OS X 10.14+  | [Keil Miller Jr](https://gitlab.com/keilmillerjr)
suppression | s | Remove the compatibility/disclaimer warning screen | [MKChamp](http://mamestuff.lowtrucks.net/MKChamp/)
various fixes | v | Extra modifications imported from [ARCADE](http://arcade.mameworld.info) (new derrative based on MAMEUIFX) | [haynor666](http://forum.arcadecontrols.com/index.php/topic,154799.0.html)

#### d3d8

> D3D8 fix is added solution for bypassing message "non-power-of-two textures" for old DX8 video cards.
>
> [source](http://forum.arcadecontrols.com/index.php/topic,154799.msg1671575.html#msg1671575)

#### d3d9ex

> Groovy MAME 0.170 under Windows 7 can use Direct Draw, Direct 3-D 9 and Direct 3-D 9 Ex, being the latter the recommended API. Notice that Direct Draw stopped being supported by MAME in version 0.171, whereas Direct 3-D 9 Ex only is supported by Groovy MAME 0.167 onwards.
>
>The advantage of Direct 3-D 9 Ex over Direct 3-D 9 is that, unlike with the latter, there's no need to enable frame delay in order to force the frame latency to the minimum allowed by the driver and therefore avoid the dreaded frame queues present in the ATI video drivers when Direct 3-D is used, which add a lag of 2-3 frames.
>
> [source](http://geedorah.com/eiusdemmodi/forum/viewtopic.php?pid=985#p985)

#### groovymame

> GroovyMAME is a M.A.M.E. fork aimed at CRT monitors, with a strong focus on CRT preservation, since this is the only display technology that accurately replicates the genuine video game experience. However you can use GroovyMAME to alleviate some of the annoyances associated to emulation on LCD displays, specially for those models which are capable of refreshing at custom rates.
>
> [source](http://forum.arcadecontrols.com/index.php/topic,151459.msg1583290.html#msg1583290)

#### osx min 10.6

> This also happens with upstream GENie, and I've found the problem. Mojave, as you may know, is Snow Leopard 2: Electric Boogaloo. Except instead of stripping PowerPC from everything it strips 32-bit i686 from everything. Part of this is that you can no longer build targeting pre-Snow Leopard OSes, and GENie attempts to maintain functionality all the way back to Tiger.
>
> Here's the change: [github link](https://github.com/mamedev/mame/commit/bb2eaa50ffe2bfbfc8db30be78170e80f843aa1e)
>
> Mojave [10.14] removed the ability to build 32-bit and so the minimum target OS available bumped up from Tiger [10.4] to Snow Leopard [10.6]. This should work all the way back to Snow Leopard [10.6], compiler permitting.
>
> [source](https://forums.bannister.org/ubbthreads.php?ubb=printthread&Board=1&main=8895&type=thread)

#### suppression

The official MAME has "skip_gameinfo" feature built in. It will skip the game info screen. The suppression patch, also known as nonag, will also remove the compatibility/disclaimer warning screen when used with the previously mentioned option. Using a binary with this patch applied is not recommended when reporting bugs or issues.

#### various fixes

These modifications are current for the MAME 0.211 patch.
 
> Video modifications:
>
> - hack for fixing sprites in neogeo driver (at the moment title screen in sengoku2 is a bit broken)
> - removed custom position and size in astrocde and cidelsa drivers (looks very bad on TV/Arcade monitor)
> - restored resolution 256x240 (from 240x240) in remaining games in btime driver as well in scregg driver
> - video fix for Metamorpic Force (proper offsets)
> - small colours fix in nemesis driver (games have now slightly more brighter picture, seems to be more accurate)
> - visible area modifications in in ssv driver (changed Air Blade; Dyna Gear)
> - visible area modifications for many games in neogeo driver (mostly from 320x224 to 304x224 but for four games even to 288x224 - Zed Blade, Goal!Goal!Goal!, Gururin, Zupapa)
> - visible area modification for X-Men,
> - visible area modification Super Contra, Gang Busters, Thunder Cross, Teenage Mutant Hero Turtles, Bells'n'Whistles [not in ARCADE32/64]
> - visible area modification for Battle Rangers, Double Dragon, Double Dragon II, Thunder Strike, China Gate, Garyo Retsuden [not in ARCADE32/64]
> - visible area modification for Express Raider, Exzisus, Liberation, Nova 2001, Rock'n'Rage, Riot City, Wonder Boy 3, Aurail [not in ARCADE32/64]
> - visible area modification for Mad Crasher, Gladiator 1984, Super Cross II, Jungle King, Sea Fighter Poseidon, Xain'n Sleena [not in ARCADE32/64]
> - visible area modification for many games in namcona1 driver [not in ARCADE32/64]
> - visible area modification for many games in system1 driver [not in ARCADE32/64]
> - some video fixes in taito_f3 driver (continue screen in Greed Seeker is now visible, the same goes to intro text in Arabian Nights)
>
> Misc modifications:
>
> - slow rendering fix for namconb1
> - more accurate video timings in Green Beret
> - swapped some IRQ handling in megasys1 driver
>
> [source](http://forum.arcadecontrols.com/index.php/topic,154799.msg1623604.html#msg1623604)

## Patching Sequence:

Patches are applied to [MAME](https://github.com/mamedev/mame) in the following order:

1. OS X Min 10.6
2. GroovyMame
3. Video mode
    * d3d8
    * d3d9ex
4. Suppression
5. Various Fixes
